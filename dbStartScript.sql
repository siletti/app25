CREATE DATABASE  IF NOT EXISTS `app25db`;
USE `app25db`;


DROP TABLE IF EXISTS `employee`;

CREATE TABLE `employee` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

--
-- Data for table `employee`
--

INSERT INTO `employee` VALUES
	(1,'Alberto','Rossi','alberto@gmail.com'),
	(2,'Marco','Bianchi','marco@gmail.com'),
	(3,'Susy','Pinco','susy@gmail.com'),
	(4,'Marina','Remo','marina@gmail.com'),
	(5,'Dodo','Verdi','dodo@gmail.com');
