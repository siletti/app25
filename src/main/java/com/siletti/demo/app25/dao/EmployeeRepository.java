package com.siletti.demo.app25.dao;

import com.siletti.demo.app25.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface EmployeeRepository extends JpaRepository<Employee, Integer> {

     List<Employee> findAllByOrderByLastNameAsc();

}
