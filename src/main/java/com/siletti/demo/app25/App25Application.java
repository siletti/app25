package com.siletti.demo.app25;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class App25Application {

    public static void main(String[] args) {
        SpringApplication.run(App25Application.class, args);
    }

}
